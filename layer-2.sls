;; -*- mode: scheme; coding: utf-8 -*-
;; Copyright © 2018 Göran Weinholt <goran@weinholt.se>
;; SPDX-License-Identifier: GPL-3.0-or-later

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
#!r6rs

;; DARC Layer 2 code.

;; TODO: Some obvious improvements should be possible through using s8
;; samples from layer 1 instead of 0/1.

(library (darc layer-2)
  (export
    get-raw-block
    read-raw-frame
    raw-block-error-correction!
    raw-frame-error-correction!
    parity-fix!)
  (import
    (except (rnrs (6)) vector-fill!)
    (only (srfi :1 lists) iota)
    (only (srfi :43 vectors) vector-copy! vector-fill!)
    (industria bytevectors)
    (industria hexdump)
    (darc checksums))

(define *debug* #f)

(define (print . x*)
  (for-each (lambda (x) (display x (current-error-port)))
            x*)
  (newline (current-error-port)))

;; Block Identification Code
(define BIC1 #vu8(0 0 0 1 0 0 1 1 0 1 0 1 1 1 1 0))
(define BIC2 #vu8(0 1 1 1 0 1 0 0 1 0 1 0 0 1 1 0))
(define BIC3 #vu8(1 0 1 0 0 1 1 1 1 0 0 1 0 0 0 1))
(define BIC4 #vu8(1 1 0 0 1 0 0 0 0 1 1 1 0 1 0 1))

(define BLOCK-BIC-SIZE 16)
(define BLOCK-PAYLOAD-SIZE 176)
(define BLOCK-CRC-SIZE 14)
(define BLOCK-PARITY-SIZE 82)
(define BLOCK-SIZE (+ BLOCK-BIC-SIZE BLOCK-PAYLOAD-SIZE BLOCK-CRC-SIZE BLOCK-PARITY-SIZE))
(define FRAME-BLOCKS 272)
(define FRAME-SIZE (* FRAME-BLOCKS BLOCK-SIZE))
(define (frame-block-offset idx) (fx* idx 288))

(define (bic-errors bic bv offset)
  (do ((i 0 (fx+ i 1))
       (e 0 (fx+ e (fxxor (bytevector-u8-ref bic i)
                          (bytevector-u8-ref bv (fx+ i offset))))))
      ((fx=? i 16)
       e)))

(define (almost-right-bic? bv offset errors)
  ;; Close enough to one of the BICs. TODO: which order should be preferred?
  (or (and (fx<=? (bic-errors BIC4 bv offset) errors) 'bic4)
      (and (fx<=? (bic-errors BIC1 bv offset) errors) 'bic1)
      (and (fx<=? (bic-errors BIC2 bv offset) errors) 'bic2)
      (and (fx<=? (bic-errors BIC3 bv offset) errors) 'bic3)
      ))

(define (closest-bic bv offset errors)
  ;; Closest by hamming distance.
  (let ((e1 (bic-errors BIC1 bv offset))
        (e2 (bic-errors BIC2 bv offset))
        (e3 (bic-errors BIC3 bv offset))
        (e4 (bic-errors BIC4 bv offset)))
    (let ((m (fxmin e1 e2 e3 e4)))
      (cond ((fx>? m errors) #f)
            ((eqv? e1 m) 'bic1)
            ((eqv? e2 m) 'bic2)
            ((eqv? e3 m) 'bic3)
            (else 'bic4)))))

(define (integer->bits r width)
  (do ((bv (make-bytevector width))
       (i (fx- width 1) (fx- i 1))
       (r r (bitwise-arithmetic-shift-right r 1)))
      ((fx=? i -1) bv)
    (bytevector-u8-set! bv i (bitwise-and r 1))))

;;; Parity

;; The data-ref and data-set! procedures are used to access 272
;; information bits.
(define (parity-fix! data-ref data-set!)
  (define (ref shift j)
    (let ((idx (fxmod (fx+ shift (fx- 272 j)) 273)))
      (if (fx<? idx 272)
          (data-ref idx)
          0)))
  (define (XOR shift is)
    (do ((i 0 (fx+ i 1))
         (s 0 (fxxor s (ref shift (vector-ref is i)))))
        ((fx=? i (vector-length is))
         s)))
  (define W
    '#(#(272 254 248 226 222 205 169 160 157 146 144 113 106 105 86 76 71)
       #(272 266 244 240 223 187 178 175 164 162 131 124 123 104 94 89 17)
       #(272 250 246 229 193 184 181 170 168 137 130 129 110 100 95 23 5)
       #(272 268 251 215 206 203 192 190 159 152 151 132 122 117 45 27 21)
       #(272 255 219 210 207 196 194 163 156 155 136 126 121 49 31 25 3)
       #(272 236 227 224 213 211 180 173 172 153 143 138 66 48 42 20 16)
       #(272 263 260 249 247 216 209 208 189 179 174 102 84 78 56 52 35)
       #(272 269 258 256 225 218 217 198 188 183 111 93 87 65 61 44 8)
       #(272 261 259 228 221 220 201 191 186 114 96 90 68 64 47 11 2)
       #(272 270 239 232 231 212 202 197 125 107 101 79 75 58 22 13 10)
       #(272 241 234 233 214 204 199 127 109 103 81 77 60 24 15 12 1)
       #(272 265 264 245 235 230 158 140 134 112 108 91 55 46 43 32 30)
       #(272 271 252 242 237 165 147 141 119 115 98 62 53 50 39 37 6)
       #(272 253 243 238 166 148 142 120 116 99 63 54 51 40 38 7 0)
       #(272 262 257 185 167 161 139 135 118 82 73 70 59 57 26 19 18)
       #(272 267 195 177 171 149 145 128 92 83 80 69 67 36 29 28 9)
       #(272 200 182 176 154 150 133 97 88 85 74 72 41 34 33 14 4)))
  (define T 17/2)
  ;; Correct all bits, including parity bits.
  (do ((shift 0 (fx+ shift 1)))
      ((fx=? shift 272))
    (do ((i 0 (fx+ i 1))
         (sum 0 (fx+ sum (XOR shift (vector-ref W i)))))
        ((fx=? i (vector-length W))
         ;; (display (list 'sum: sum))
         ;; (newline)
         (when (> sum T)                ;FIXME: iterative, and exact
           ;; (print "CORRECTION at shift " shift)
           (data-set! shift (fxxor (data-ref shift) 1)))))))

;;; Pseudo noise scrambler

;; g(x) = x^9 + x^4 + 1, start=101010101
(define (gen-PN n)
  (call-with-bytevector-output-port
   (lambda (p)
     (let lp ((i 0) (crc8 1) (crc7 0) (crc6 1) (crc5 0)
              (crc4 1) (crc3 0) (crc2 1) (crc1 0) (crc0 1))
       (unless (fx=? i n)
         (let ((inv crc8))
           (put-u8 p inv)
           (lp (fx+ i 1) crc7 crc6 crc5 crc4 (fxxor crc3 inv)
               crc2 crc1 crc0 inv)))))))

(define PN (gen-PN (- BLOCK-SIZE 16)))

;; Output from the above, sent to windytan on Freenode after her first
;; DARC blog post:

;; #(1 0 1 0 1 1 1 1 1 0 1 0 1 0 1 0 1 0 0 0 0 0 0 1 0 1 0 0 1
;;   0 1 0 1 1 1 1 0 0 1 0 1 1 1 0 1 1 1 0 0 0 0 0 0 1 1 1 0 0
;;   1 1 1 0 1 0 0 1 0 0 1 1 1 1 0 1 0 1 1 1 0 1 0 1 0 0 0 1 0
;;   0 1 0 0 0 0 1 1 0 0 1 1 1 0 0 0 0 1 0 1 1 1 1 0 1 1 0 1 1
;;   0 0 1 1 0 1 0 0 0 0 1 1 1 0 1 1 1 1 0 0 0 0 1 1 1 1 1 1 1
;;   1 1 0 0 0 0 0 1 1 1 1 0 1 1 1 1 1 0 0 0 1 0 1 1 1 0 0 1 1
;;   0 0 1 0 0 0 0 0 1 0 0 1 0 1 0 0 1 1 1 0 1 1 0 1 0 0 0 1 1
;;   1 1 0 0 1 1 1 1 1 0 0 1 1 0 1 1 0 0 0 1 0 1 0 1 0 0 1 0 0
;;   0 1 1 1 0 0 0 1 1 0 1 1 0 1 0 1 0 1 1 1 0 0 0 1 0 0 1 1 0
;;   0 0 1 0 0 0 1 0 0 0 0 0 0 0 0 1 0 0 0 0 1 0 0 0 1 1 0 0 0
;;   0 1 0 0 1 1 1 0 0 1)

(define (descramble! x)
  ;; g(x) = x^9 + x^4 + 1
  (define fxasr fxarithmetic-shift-right)
  (define fxasl fxarithmetic-shift-left)
  ;; (define g #b1000010001)
  ;; (define start #b101010101)
  (do ((i 0 (+ i 1)))
      ((= i (+ 176 14 82)))
    (let ((s-i (+ i 16)))
      (bytevector-u8-set! x s-i
                          (fxxor (bytevector-u8-ref x s-i)
                                 (bytevector-u8-ref PN i))))))

;;;

;; Converts unpacked bits (one bit per byte) to packed.
(define (bits->bytes bv)
  (let ((in (open-bytevector-input-port bv)))
    (call-with-bytevector-output-port
      (lambda (out)
        (let lp ()
          (unless (port-eof? in)
            (let* ((b0 (get-u8 in))
                   (b1 (get-u8 in))
                   (b2 (get-u8 in))
                   (b3 (get-u8 in))
                   (b4 (get-u8 in))
                   (b5 (get-u8 in))
                   (b6 (get-u8 in))
                   (b7 (get-u8 in)))
              (put-u8 out
                      (fxior (fxarithmetic-shift-left b0 0)
                             (fxarithmetic-shift-left b1 1)
                             (fxarithmetic-shift-left b2 2)
                             (fxarithmetic-shift-left b3 3)
                             (fxarithmetic-shift-left b4 4)
                             (fxarithmetic-shift-left b5 5)
                             (fxarithmetic-shift-left b6 6)
                             (fxarithmetic-shift-left b7 7)))
              (lp))))))))

;; Reads bits until there is a BIC. Returns the closest BIC, the whole
;; block and the number of undecoded bits.
(define (get-raw-block port)
  (define acceptable-errors 2)
  (define bic-size (bytevector-length BIC1))
  (let ((bv (get-bytevector-n port bic-size)))
    (let lp ((skipped-bits 0))
      (cond
        ((eof-object? bv)
         (values #f (eof-object) skipped-bits))
        ((not (fx=? (bytevector-length bv) bic-size))
         (values #f (eof-object) (+ skipped-bits (bytevector-length bv))))
        ((almost-right-bic? bv 0 acceptable-errors)
         (let ((block-data (get-bytevector-n port (- BLOCK-SIZE bic-size))))
           (cond
             ((or (eof-object? block-data)
                  (not (= (bytevector-length block-data) (- BLOCK-SIZE bic-size))))
              (values #f (eof-object) (+ skipped-bits (bytevector-length bv) bic-size)))
             (else
              (let* ((block (bytevector-append bv block-data))
                     (bic (closest-bic block 0 acceptable-errors)))
                (descramble! block)
                (when *debug*
                  (let ((blk (bits->bytes block)))
                    (hexdump (current-error-port) blk 0 (bytevector-length blk)
                             (string-append (if bic (symbol->string bic) "----") " B "))))
                ;; (hexdump (current-error-port) block 0 (bytevector-length block)
                ;;          (string-append (if bic (symbol->string bic) "----") " b "))
                (values bic block skipped-bits))))))
        (else
         ;; Shift out the oldest bit.
         (bytevector-copy! bv 1 bv 0 (- bic-size 1))
         (let ((byte (get-u8 port)))
           (cond ((eof-object? byte)
                  (values #f (eof-object) skipped-bits))
                 (else
                  (bytevector-u8-set! bv (- bic-size 1) byte)
                  (lp (fx+ skipped-bits 1))))))))))

(define (compress-block-types block-types)
  (define (add current count acc)
    (if (zero? count)
        acc
        (cons (cons current count) acc)))
  (let lp ((i 0) (current #f) (count 0) (acc '()))
    (cond ((fx=? i (vector-length block-types))
           (reverse (add current count acc)))
          ((eqv? (vector-ref block-types i) current)
           (lp (+ i 1) (vector-ref block-types i) (+ count 1) acc))
          (else
           (lp (+ i 1) (vector-ref block-types i) 1 (add current count acc))))))

;; Finds the start of a frame.
(define (find-start-of-frame block-types)
  ;; TODO: Buffer more than a frame and then try to align the various
  ;; frame types with what is in the buffer.
  (print "SYNCING " (compress-block-types block-types))
  (let lp ((i 0) (count1 0) (count2 0) (count3 0) (count4 0))
    (print (list i 'bic1: count1 'bic2: count2 'bic3: count3 'bic4: count4))
    (cond ((fx>? i (fx- (vector-length block-types) 70))
           (print "SYNC FAILED")
           (+ count4 count1))                          ;just skip some blocks
          ;; XXX: sample 60 blocks
          ((and (fx<=? 60 count3 62) (fx<=? 70 count2 72))
           (- i count3 count2))

          ((and (fx=?  count3 60))
           (- i count3))

          #;
          ((fx>? count1 30)
           (lp (fx+ i 1) 0 0 0 0))

          (else
           (let ((bic (vector-ref block-types i)))
             (lp (fx+ i 1)
                 (fx+ count1 (if (eq? bic 'bic1) 1 0))
                 (fx+ count2 (if (eq? bic 'bic2) 1 0))
                 (fx+ count3 (if (eq? bic 'bic3) 1 0))
                 (fx+ count4 (if (eq? bic 'bic4) 1 0))))))))

;; Reads a raw frame from the port. The callback is called with
;; arguments: raw-block bic idx realtime?.
(define (read-raw-frame port block-callback)
  (define frame (make-bytevector FRAME-SIZE 0))
  (define block-types (make-vector FRAME-BLOCKS #f))
  (define (return-block bits realtime?)
    (let ((bytes (bits->bytes bits)))
      (block-callback (subbytevector bytes 0 24) realtime?)))
  (define (return-blocks start-idx end-idx)
    (let ((bits (make-bytevector BLOCK-SIZE)))
      (do ((i start-idx (fx+ i 1)))
          ((fx=? i end-idx))
        (bytevector-copy! frame (frame-block-offset i) bits 0 BLOCK-SIZE)
        (return-block bits #f))))
  ;; Frames are made up of 272 blocks.
  ;; Frame type A0 is 60*BIC3, 70*BIC2, 60*BIC1, 82*BIC4.
  ;; Frame type A1 has extra BIC2 blocks mixed in with BIC4.
  ;; Frame type B interleaves blocks in some weird pattern.
  ;; Frame type C is only BIC3 blocks.
  (let lp ((block-idx 0) (prev-bic #f))
    (let-values (((bic block skipped-bits) (get-raw-block port)))
      (unless (zero? skipped-bits)
        (print "skipped " skipped-bits " bits"))
      (cond
        ((eof-object? block))
        ((and (= block-idx 0) (> skipped-bits 0))
         (lp block-idx prev-bic))
        (else
         (let ((realtime? (and (eq? bic 'bic2) (eq? prev-bic 'bic4) 'realtime))
               (block-idx (if (fx>? skipped-bits 50)
                              ;; Some bits were not decodable, insert
                              ;; a dummy block in hope of syncing up.
                              ;; XXX: there is something stupid about
                              ;; this.
                              (fxmin 271 (+ block-idx (+ 1 (div skipped-bits BLOCK-SIZE))))
                              block-idx)))
           (cond
             (realtime?
              (print "FRAME A1 real-time data")
              (raw-block-error-correction! block 0)
              (return-block block realtime?)
              (lp block-idx prev-bic))
             (else
              (bytevector-copy! block 0 frame (frame-block-offset block-idx) BLOCK-SIZE)
              (vector-set! block-types block-idx bic)
              (cond
                ((fx=? block-idx 271)   ;full frame
                 ;; TODO: Frame A has vertical and hortizontal parity.
                 ;; Frame B has something weird. Frame C has only horizontal.
                 #;
                 (let ((frame (bits->bytes frame)))
                   (hexdump (current-error-port) frame 0 (bytevector-length frame) "SYNCING "))
                 (let ((start-idx (find-start-of-frame block-types)))
                   (print "READ ALL DATA FOR A FRAME, SYNC'D TO: " start-idx)
                   (cond
                     ((zero? start-idx)
                      (raw-frame-error-correction! frame)
                      (return-blocks 0 FRAME-BLOCKS)
                      #t)
                     (else
                      ;; Resync with the frame start.
                      (print "RESYNCING FRAME")
                      (return-blocks 0 start-idx)
                      (bytevector-copy! frame 0 frame (frame-block-offset start-idx)
                                        (- FRAME-SIZE (frame-block-offset start-idx)))
                      (vector-copy! block-types 0 block-types start-idx)
                      (vector-fill! block-types #f (- FRAME-BLOCKS start-idx))
                      (print "RESYNC " (compress-block-types block-types))
                      #;
                      (let ((frame (bits->bytes frame)))
                        (hexdump (current-error-port) frame 0 (bytevector-length frame) "RESYNC "))
                      (lp (- FRAME-BLOCKS start-idx) bic)))))
                (else
                 (lp (fx+ block-idx 1) bic)))))))))))

;; Verify the 14-bit L2 CRC. Offset points to just after the BIC.
(define (raw-block-verify-checksum bits offset)
  (let ((l2crc (subbytevector bits (fx+ offset 176) (fx+ offset (+ 176 14))))
        (info-crc (integer->bits (crc-14/darc-finish
                                  (crc-14/darc-update (crc-14/darc-init)
                                                      bits offset
                                                      (fx+ offset 176)))
                                 (crc-14/darc-width))))
    (bytevector=? info-crc l2crc)))

(define (raw-block-error-correction! bits offset)
  ;; The ETSI document is wrong about the block layout of BIC4. All
  ;; blocks have the same layout wrt to CRC and parity.
  (unless (raw-block-verify-checksum bits (fx+ offset 16))
    ;; XXX: It is possible for the 14-bit crc to match but the
    ;; horizontal parity would still fix some bit errors. It's also
    ;; possible for the parity to mess up a block with a good crc.
    (print "applying parity at offset " offset)
    (let ((block (subbytevector bits (fx+ offset 16) (fx+ offset BLOCK-SIZE))))
      (define (block-ref idx) (bytevector-u8-ref block idx))
      (define (block-set! idx v) (bytevector-u8-set! block idx v))
      (parity-fix! block-ref block-set!)
      (cond ((raw-block-verify-checksum block 0)
             ;; Use the fixed block.
             (bytevector-copy! block 0 bits (fx+ offset 16) (bytevector-length block)))
            ((eq? (closest-bic bits offset 2) 'bic4)
             (print "parity FAILED on BIC4"))
            (else
             (print "parity FAILED"))))))

;; Is the CRC of every block in this frame ok?
(define (raw-frame-verify-checksum bits)
  (let lp ((i 0))
    (let ((offset (frame-block-offset i)))
      (cond
        ((fx=? i FRAME-BLOCKS)
         #t)
        ((eq? (closest-bic bits offset 2) 'bic4) ;do not care about BIC4 CRC
         (lp (fx+ i 1)))
        (else
         (and (raw-block-verify-checksum bits (fx+ (frame-block-offset i) 16))
              (lp (fx+ i 1))))))))

(define (raw-frame-vertical-parity-fix! bits)
  (do ((column 0 (fx+ column 1)))
      ((fx=? column FRAME-BLOCKS))
    (let ()
      (define (index idx)
        (fx+ (fx+ 16 column) (fx* idx BLOCK-SIZE)))
      (define (block-ref idx)
        (bytevector-u8-ref bits (index idx)))
      (define (block-set! idx v)
        (cond ((raw-block-verify-checksum bits (fx+ (frame-block-offset idx) 16))
               #;(print "Ignoring fix to " column "×" idx)
               )
              (else
               #;(print "Applying fix to " column "×" idx)
               (bytevector-u8-set! bits (index idx) v)
               (raw-block-error-correction! bits (frame-block-offset idx)))))
      (parity-fix! block-ref block-set!))))

;; Fix the frame using the parity blocks.
(define (raw-frame-error-correction! bits)
  (print)
  (print "Time for error correction")
  (print)

  ;; Apply horizontal parity. TODO: Use the vertical-parity code. Just
  ;; rotate and use the crc82 instead of the crc14?
  (do ((i 0 (fx+ i 1)))
      ((fx=? i FRAME-BLOCKS))
    (raw-block-error-correction! bits (frame-block-offset i)))

  ;; Did it work?
  (cond ((raw-frame-verify-checksum bits)
         (print "FRAME OK"))
        (else
         ;; Run the parity correction vertically. TODO: frame B needs
         ;; to be sorted to have parity blocks last. Block C does not
         ;; work with this at all.
         (print "Time for VERTICAL")
         (raw-frame-vertical-parity-fix! bits)
         (if (raw-frame-verify-checksum bits)
             (print "VERTICAL PARITY SUCCESS - FRAME OK")
             (print "VERTICAL PARITY FAILED"))))
  #;
  (print "OK BLOCKS:"
         (length
          (filter (lambda (i)
                    (raw-block-verify-checksum bits (fx+ (frame-block-offset i) 16)))
                  (iota FRAME-BLOCKS)))))


)
