;; -*- mode: scheme; coding: utf-8 -*-
;; Copyright © 2018 Göran Weinholt <goran@weinholt.se>
;; SPDX-License-Identifier: MIT

;; CRCs used in DARC.

(library (darc checksums)
  (export
    crc-6/darc crc-6/darc-init crc-6/darc-finish crc-6/darc-self-test crc-6/darc-width crc-6/darc-update
    crc-8/darc crc-8/darc-init crc-8/darc-finish crc-8/darc-self-test crc-8/darc-width crc-8/darc-update
    crc-14/darc crc-14/darc-init crc-14/darc-finish crc-14/darc-self-test crc-14/darc-width crc-14/darc-update
    crc-16/genibus crc-16/genibus-init crc-16/genibus-finish crc-16/genibus-self-test crc-16/genibus-width crc-16/genibus-update
    crc-82/darc crc-82/darc-init crc-82/darc-finish crc-82/darc-self-test crc-82/darc-width crc-82/darc-update)
  (import
    (rnrs (6))
    (hashing crc))

(define-bit-oriented-crc crc-6/darc
  (polynomial (6 4 3 0))
  (init 0)
  (ref-in #t)
  (ref-out #t)
  (xor-out 0)
  (check #x26))

(define-crc crc-8/darc
  (polynomial (8 5 4 3 0))
  (init 0)
  (ref-in #t)
  (ref-out #t)
  (xor-out 0)
  (check #x15))

(define-bit-oriented-crc crc-14/darc
  (polynomial (14 11 2 0))
  (init 0)
  (ref-in #t)
  (ref-out #t)
  (xor-out 0)
  (check #x82d))

(define-crc crc-16/genibus
  (polynomial (16 12 5 0))
  (init #xffff)
  (ref-in #f)
  (ref-out #f)
  (xor-out #xffff)
  (check #xd64e))

(define-bit-oriented-crc crc-82/darc
  (polynomial (82 77 76 71 67 66 56 52 48 40 36 34 24 22 18 10 4 0))
  (init 0)
  (ref-in #t)
  (ref-out #t)
  (xor-out 0)
  (check #x09ea83f625023801fd612)))
