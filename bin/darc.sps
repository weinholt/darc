#!/usr/bin/env scheme-script
;; -*- mode: scheme; coding: utf-8 -*- !#
;; Copyright © 2018 Göran Weinholt <goran@weinholt.se>
;; SPDX-License-Identifier: GPL-3.0-or-later
#!r6rs

;; Reads bytes from the GMSK demod block and spits out BIC+payload
;; blocks in the wrong bit order.

(import
  (rnrs (6))
  ;;(industria hexdump)
  (darc layer-2)
  (only (chezscheme) sleep make-time))

(define (main)
  (unless (= (length (command-line)) 1)
    (error 'main "Usage: nc -u -l -p 51338 | darc.sps | nc --buffer-size=24 -u localhost 51337"))
  (call-with-port (standard-input-port)
    (lambda (inp)
      (call-with-port (open-file-output-port "/dev/stdout" (file-options no-fail)
                                             (buffer-mode none))
        (lambda (outp)
          (define (block-callback bytes realtime?)
            ;; (hexdump (current-error-port) bytes 0 (bytevector-length bytes)
            ;;          (if realtime? "R " "B "))
            (do ((i 0 (fx+ i 1)))
                ((fx=? i (bytevector-length bytes)))
              ;; Compatibility with muccc/darc
              (let ((byte (bytevector-u8-ref bytes i)))
                (bytevector-u8-set! bytes i (fxreverse-bit-field byte 0 8))))
            (put-bytevector outp bytes)
            (flush-output-port outp)
            (sleep (make-time 'time-duration 10000000 0)))
          (let lp ()
            (let ((n (read-raw-frame inp block-callback)))
              (unless (eof-object? n)
                (lp)))))))))




(main)
